/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <iomanip>
#include <sstream>
#include <algorithm>

#include "mauve/runtime/logging/mauve_formatter.hpp"
#include "mauve/runtime/Component.hpp"

namespace mauve {
  namespace runtime {

void mauve_formatter::format(spdlog::details::log_msg& msg) {
  // Log level
  std::ostringstream l5;
  l5 << std::setw(8) << spdlog::level::to_str(msg.level);
  std::string str = l5.str();
  std::transform(str.begin(), str.end(),str.begin(), ::toupper);
  msg.formatted << "[" << str << "]";
  msg.formatted << " ";
  // User message
  msg.formatted << fmt::StringRef(msg.raw.data(), msg.raw.size());
  msg.formatted.write(spdlog::details::os::eol, spdlog::details::os::eol_size);
}

}}
