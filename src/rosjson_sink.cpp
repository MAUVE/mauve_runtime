/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <sstream>
#include <chrono>
#include <ctime>

#include "mauve/runtime/logging/rosjson.hpp"

namespace mauve {
  namespace runtime {

    rosjson_sink::rosjson_sink(const std::string& server_host, int server_port) {
      sockfd = socket(PF_INET, SOCK_DGRAM, 0);
      serv.sin_family = AF_INET;
      serv.sin_port = htons(server_port);
      serv.sin_addr.s_addr = inet_addr(server_host.c_str());
    }

    void rosjson_sink::log(const spdlog::details::log_msg& msg) {
      std::stringstream s;
      s << '{';
      s << "\"op\": \"publish\",";
      s << "\"topic\": \"/rosout\",";
      s << "\"msg\": {";
      //time_t t = time(NULL);//spdlog::log_clock::to_time_t(msg.time);
      //s << "'header': { 'seq': 0, 'frame_id': '', 'stamp': " << t << "},";
      int lvl = 0;
      switch (msg.level) {
        case spdlog::level::trace:
        case spdlog::level::debug:
          lvl = 1; break;
        case spdlog::level::info:
          lvl = 2; break;
        case spdlog::level::warn:
          lvl = 4; break;
        case spdlog::level::err:
          lvl = 8; break;
        case spdlog::level::critical:
          lvl = 16; break;
        case spdlog::level::off:
          lvl = 0; break;
      };
      s << "\"level\":" << lvl << ",";
      std::string name(* msg.logger_name); name.pop_back();
      s << "\"name\": \"" << name << "\",";
      std::string m; m = msg.formatted.str(); m.pop_back();
      s << "\"msg\": \"" << m << "\",";
      s << "\"line\": " << (unsigned int)msg.thread_id;
      s << "}}";
      sendto(sockfd, s.str().c_str(), s.str().size(), 0,
        reinterpret_cast<const sockaddr*>(&serv), sizeof(serv));
    }

  }
}
