/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/Property.hpp"
#include "mauve/runtime/HasProperty.hpp"

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  StringProperty::StringProperty(HasProperty* container, std::string const & name, std::string init_value)
  : AbstractProperty { container, name}
  , value            { init_value }
  {}

  // ------------------------- Destructor -------------------------

  StringProperty::~StringProperty() noexcept {}

  // ------------------------- Method -------------------------

  property_type StringProperty::get_type() const {
    return STRING;
  }

  std::string StringProperty::type_name() const {
    return "std::string";
  }

  std::string StringProperty::get_value() const {
    return value;
  }

  bool StringProperty::set_value(std::string const & value) {
    if (container->is_configured()) {
      return false;
    }
    this->value = value;
    return true;
  }

  // ------------------------- Operator -------------------------

  StringProperty::operator std::string&() {
    return value;
  }

  StringProperty::operator std::string() const {
    return value;
  }

  StringProperty & StringProperty::operator=(std::string const & value) {
    set_value(value);
    return *this;
  }

  StringProperty & StringProperty::operator=(StringProperty const & other) {
    set_value(other.value);
    return *this;
  }

}}
