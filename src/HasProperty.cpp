/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/HasProperty.hpp"
#include "mauve/runtime/Property.hpp"

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    HasProperty::HasProperty() {}

    // ------------------------- Destructor -------------------------

    HasProperty::~HasProperty() noexcept {
      for (AbstractProperty* property: properties) {
        delete property;
      }
    }

    // ------------------------- Method -------------------------

    const std::vector<AbstractProperty*> HasProperty::get_properties() const {
      std::vector<AbstractProperty*> vect;
      for (AbstractProperty* property: properties) {
        vect.push_back(property);
      }
      return vect;
    }

    AbstractProperty* HasProperty::get_property(std::string const & name) const {
      for (AbstractProperty* property: properties) {
        if (property->name == name) {
          return property;
        }
      }
      return nullptr;
    }

    AbstractProperty* HasProperty::get_property(int index) const {
      if (index < 0 || (unsigned)index >= properties.size()) return nullptr;
      return properties[(unsigned)index];
    }

  }
}
