/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/Shell.hpp"
#include "mauve/runtime/logging/logger.hpp"

#include <iostream>
#include <typeinfo>
#include <cxxabi.h>

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    Shell::Shell()
    : HasProperty {}
    , HasPort     {}
    , _container  { nullptr }
    , _configured { false }
    {
    }

    // ------------------------- Destructor -------------------------

    Shell::~Shell() noexcept {
      _cleanup();
    }

    // ------------------------- Method -------------------------

    std::string Shell::type_name() const {
      int status = -4;
      return abi::__cxa_demangle(typeid(*this).name(), nullptr, nullptr, &status);
    }

    void Shell::print_model(std::ostream& out) const {
      out << "\tshell " << type_name() << " {" << std::endl;
      out << "\t}" << std::endl;
    }

    bool Shell::_configure() {
      if (is_configured()) return true;
      _configured = configure_hook();
      return _configured;
    }

    void Shell::_cleanup() {
      if (is_configured()) {
        cleanup_hook();
        _configured = false;
      }
    }

    // ------------------------- Function -------------------------

    std::ostream& operator<<(std::ostream& out, Shell const & shell) {
      out << shell.type_name();
      return out;
    }

  }
} /* namespace mauve */
