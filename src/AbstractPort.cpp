/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/AbstractPort.hpp"
#include <typeinfo>
#include <cxxabi.h>

namespace mauve {
  namespace runtime {

  // -------------------- Constructor --------------------

  AbstractPort::AbstractPort(std::string const & name)
  : name { name }
  {}

  // -------------------- Destructor --------------------

  AbstractPort::~AbstractPort() noexcept {}

  // -------------------- Method --------------------

}} /* namespace mauve */
