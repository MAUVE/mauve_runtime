/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime.hpp"
#include <iostream>
#include <type_traits>

using namespace mauve::runtime;

struct A { int x; };

struct S: public Shell {
  Property<int> & i = mk_property<int>("i", 1);
  Property<double> & d = mk_property<double>("d", 1.2);
  Property<std::string> & s = mk_property<std::string>("s", "coucou");
  Property<A> & a = mk_property<A>("a", A());
};

int main(int argc, char const *argv[]) {
  S shell;

  std::cout << shell.i.name << " = " << shell.i.get_value() << std::endl;
  std::cout << shell.d.name << " = " << shell.d.get_value() << std::endl;
  std::cout << shell.s.name << " = " << shell.s.get_value() << std::endl;
  std::cout << shell.a.name << " = " << "?" << std::endl;

  shell.i = shell.i;
  std::cout << (1 + shell.i) << std::endl;
  std::cout << (1.2 + shell.d) << std::endl;
  std::string temp = "machin: ";
  temp += shell.s;
  std::cout << temp << std::endl;

  return 0;
}
