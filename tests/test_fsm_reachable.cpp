/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime.hpp"
#include <iostream>

using namespace mauve::runtime;

struct S: public Shell {};
struct C: public Core<S> {
  bool guard() {
    return true;
  }
  void update() {
    std::cout << "update" << std::endl;
  }
};
struct F: public FiniteStateMachine<S, C> {
  ExecState<C> & a = mk_execution("A", &C::update);
  ExecState<C> & b = mk_execution("B", &C::update);
  ExecState<C> & c = mk_execution("C", &C::update);
  ExecState<C> & d = mk_execution("D", &C::update);
  ExecState<C> & e = mk_execution("E", &C::update);

  bool configure_hook() override {
    set_initial(a);
    a.set_next(b);
    b.set_next(c);
    mk_transition(b, &C::guard, a);
    c.set_next(e);
    d.set_next(e);
    e.set_next(a);
    return true;
  }
};

int main(int argc, char const *argv[]) {
  F f;
  f.configure_hook();
  std::cout << "check: " << f.check_reachable() << std::endl;

  return 0;
}
