/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "Archi.hpp"
#include <iostream>

int main(int argc, char const *argv[]) {

  Archi archi;
  Deployer<Archi>* deployer = mk_deployer(&archi);
  archi.manager.core().set_deployer(deployer);

  archi.configure();
  deployer->create_tasks();

  archi.writer.set_cpu(0);
  archi.normal.set_cpu(0);
  archi.backup.set_cpu(0);
  archi.manager.set_cpu(0);

  archi.writer.set_priority(1);
  archi.normal.set_priority(2);
  archi.backup.set_priority(3);
  archi.manager.set_priority(10);


  deployer->activate();
  deployer->start();
  deployer->loop();
  deployer->stop();
}
