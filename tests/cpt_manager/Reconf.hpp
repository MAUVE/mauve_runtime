/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef RECONF_HPP
#define RECONF_HPP

#include "mauve/runtime.hpp"
#include "mauve/runtime/DataStatus.hpp"

using namespace mauve::runtime;

// ---------- Writer ----------

struct WriterShell : public Shell {
  WritePort<bool> & status = mk_port<WritePort<bool>>("status");
  WritePort<int>  & data   = mk_port<WritePort<int>> ("data");
};

struct WriterCore : public Core<WriterShell> {

  bool configure_hook() override {
    status = true;
    count = 0;
    return true;
  }

  void update() {
    count ++;
    if (count > 5) {
      status = !status;
      count = 0;
      shell().status.write(status);
    }
    shell().data.write(count);
    std::cout << container_name() << ": " << count << " - " << status << std::endl;
  }

private:
  bool status;
  int count;
};

struct WriterFSM : public FiniteStateMachine<WriterShell, WriterCore> {
  ExecState<WriterCore>    & U = mk_execution("U", &WriterCore::update);
  SynchroState<WriterCore> & W = mk_synchronization("W", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U);
    set_next(U, W);
    set_next(W, U);

    return true;
  }
};

// ---------- Reader ----------

struct ReaderShell: public Shell {
  ReadPort<int> & data = mk_port<ReadPort<int>>("data", 0);
};

struct ReaderCore: public Core<ReaderShell> {
  void update() {
    int data = shell().data.read();
    std::cout << "Reader " << container_name() << ": " << data << std::endl;
  }
};

struct ReaderFSM : public FiniteStateMachine<ReaderShell, ReaderCore> {
  ExecState<ReaderCore>    & U = mk_execution("U", &ReaderCore::update);
  SynchroState<ReaderCore> & W = mk_synchronization("W", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U);
    set_next(U, W);
    set_next(W, U);

    return true;
  }
};

// ---------- Manager ----------

struct Archi;

struct ManagerShell: public Shell {
  ReadPort<StatusValue<bool>> & status = mk_port<ReadPort<StatusValue<bool>>>("status", StatusValue<bool>{ DataStatus::NO_DATA, true });
};

struct ManagerCore: public Core<ManagerShell> {

  void update();

  void set_deployer(Deployer<Archi> * deployer) {
    _deployer = deployer;
  }

private:
  inline Deployer<Archi> & deployer() const { return *_deployer; }
  Deployer<Archi> * _deployer;
};

struct ManagerFSM : public FiniteStateMachine<ManagerShell, ManagerCore> {
  ExecState<ManagerCore>    & U = mk_execution("U", &ManagerCore::update);
  SynchroState<ManagerCore> & W = mk_synchronization("W", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U);
    set_next(U, W);
    set_next(W, U);

    return true;
  }
};

#endif
