#!/usr/bin/env python3
'''
# Copyright 2017 ONERA
#
# This file is part of the MAUVE Runtime project.
#
# MAUVE Runtime is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE Runtime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
'''
import mauve_runtime
mauve_runtime.load_deployer("devel/lib/libtest_deployer_python.so")
deployer = mauve_runtime.deployer
deployer.architecture.configure()
deployer.create_tasks()
deployer.activate()
cpt_1 = deployer.architecture.cpt_1
cpt_2 = deployer.architecture.cpt_2
data = deployer.architecture.data
t1 = deployer.task(cpt_1)
t2 = deployer.task(cpt_2)
