/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <iostream>
#include <stdint.h>

#include "mauve/runtime.hpp"

using namespace mauve::runtime;

//------------------------- Writer -------------------------

struct WS : public Shell {
  Property<int> &inc = mk_property<int>("inc", 1);
  WritePort<int> &port = mk_port<WritePort<int>>("port");
};

struct WC : public Core<WS> {
  int value = 0;

  bool configure_hook() override {
    value = 1;
    return true;
  }

  void update() {
    shell().port.write(value);
    value = value + shell().inc;
  }
};

struct WF : public FiniteStateMachine<WS, WC> {
  Property<time_ns_t> &period = this->mk_property("period", sec_to_ns(2));
  ExecState<WC> &E = mk_execution("E", &WC::update);
  SynchroState<WC> &W = mk_synchronization("W", period);

  bool configure_hook() override {
    W.set_clock(period);
    set_initial(E);
    set_next(E, W);
    set_next(W, E);
    return true;
  }
};

//------------------------- Reader -------------------------

struct RS : public Shell {
  Property<std::string> &msg = mk_property<std::string>("message", "hello");
  ReadPort<int> &port = mk_port<ReadPort<int>>("port", 0);
};

struct RC : public Core<RS> {
  int value = 0;

  void readValue() { value = shell().port.read(); }

  bool guard() { return value > 10; }

  void printMsg() {
    const std::string message = shell().msg;
    std::cout << "Reader: " << message << " value=" << value << std::endl;
  }
};

struct RF : public FiniteStateMachine<RS, RC> {
  ExecState<RC> &R = mk_execution("Read", &RC::readValue);
  ExecState<RC> &M = mk_execution("Msg", &RC::printMsg);
  SynchroState<RC> &W = mk_synchronization("W", sec_to_ns(10));

  bool configure_hook() override {
    set_initial(R);
    mk_transition(R, &RC::guard, M);
    set_next(R, W);
    set_next(M, W);
    set_next(W, R);
    return true;
  }
};

//------------------------- Architecture -------------------------

struct Archi : public Architecture {
  Component<WS, WC, WF> &writer = mk_component<WS, WC, WF>("writer");
  Component<RS, RC, RF> &reader = mk_component<RS, RC, RF>("reader");
  SharedData<int> &data = mk_resource<SharedData<int>>("data", 0);

  bool configure_hook() override {
    // writer.shell().inc = 2;

    writer.set_cpu(0);
    writer.set_priority(20);
    reader.set_cpu(0);
    reader.set_priority(10);

    writer.shell().port.connect(data.interface().write);
    reader.shell().port.connect(data.interface().read_value);

    writer.configure();
    reader.configure();

    return true;
  }
};
