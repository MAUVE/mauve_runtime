#####
# Copyright 2017 ONERA
#
# This file is part of the MAUVE Runtime project.
#
# MAUVE Runtime is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE Runtime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
cmake_minimum_required (VERSION 2.8)

# set ( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pthread -lrt -latomic" )
#set ( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread" )

#---------- ---------- ----------
#           Solvers Lib
#---------- ---------- ----------

set (TESTS_DIR ${MAUVE_DIR}/tests PARENT_SCOPE)

add_executable ( test_shared_data test_shared_data.cpp )
target_link_libraries ( test_shared_data mauve_runtime )

# add_executable ( test_shared_data_port test_shared_data_port.cpp )
# target_link_libraries ( test_shared_data_port mauve_runtime )

# add_executable ( test_multiple_port_connections test_multiple_port_connections.cpp )
# target_link_libraries ( test_multiple_port_connections mauve_runtime )

add_executable ( test_runtime test_runtime.cpp )
target_link_libraries ( test_runtime mauve_runtime )

#add_executable ( test_overload test_overload.cpp )
#target_link_libraries ( test_overload mauve_runtime )

add_executable(test_property test_property.cpp)
target_link_libraries (test_property mauve_runtime)

add_executable(test_fsm_property test_fsm_property.cpp)
target_link_libraries (test_fsm_property mauve_runtime)

add_executable(test_fsm_reachable test_fsm_reachable.cpp)
target_link_libraries (test_fsm_reachable mauve_runtime)

#add_executable(main main.cpp)
#target_link_libraries (main)

# add_executable(test_realtime test_realtime.cpp)
# target_link_libraries (test_realtime)

# add_executable(test_mutex_realtime test_mutex_realtime.cpp)
# target_link_libraries (test_mutex_realtime)

# add_executable(test_pcp_mutex test_pcp_mutex.cpp)
# target_link_libraries (test_pcp_mutex)

# add_executable(perfo_mutex perfo_mutex.cpp)
# target_link_libraries (perfo_mutex)

# add_executable(timer_test timer_test.cpp)
# target_link_libraries (timer_test)

# add_executable(test_clock test_clock.cpp)
# target_link_libraries (test_clock)

# add_executable(test_task test_task.cpp)
# target_link_libraries (test_task task)

# add_executable(test_architecture test_architecture.cpp)
# target_link_libraries (test_architecture runtime)

# add_executable(test_component test_component.cpp)
# target_link_libraries (test_component property data shell core fsm component)

# add_executable(test_connection test_connection.cpp)
# target_link_libraries (test_connection data shell core fsm component)


# add_executable(test_reconf test_reconf.cpp)
# target_link_libraries (test_reconf reconf mauve_runtime)

add_executable(test_deployer test_deployer.cpp)
target_link_libraries (test_deployer mauve_runtime)

add_library(test_deployer_python test_deployer_python.cpp)
target_link_libraries (test_deployer_python mauve_runtime)

add_library(test_deployer_interface test_deployer_interface.cpp)
target_link_libraries (test_deployer_interface mauve_runtime)

add_library(test_deployer_demo test_deployer_demo.cpp)
target_link_libraries (test_deployer_demo mauve_runtime)

# add_executable(test_monitor test_monitor.cpp)
# target_link_libraries (test_monitor monitor)

# add_executable(test_unpack test_unpack.cpp)
# target_link_libraries (test_unpack)

# add_executable(core_test core_test.cpp)
# target_link_libraries (core_test core)

# add_library (solvers ${SOLVERS_HEADERS} ${SOLVERS_INLINES} ${SOLVERS_SOURCES})
# target_link_libraries (
#     solvers
# )

add_subdirectory( cpt_manager )
