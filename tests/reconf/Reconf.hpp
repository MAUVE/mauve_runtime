/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef RECONF_HPP
#define RECONF_HPP

#include "mauve_runtime/common.hpp"
#include "mauve_runtime/Shell.hpp"
#include "mauve_runtime/ReadPort.hpp"
#include "mauve_runtime/WritePort.hpp"
#include "mauve_runtime/Core.hpp"
#include "mauve_runtime/FiniteStateMachine.hpp"
#include "mauve_runtime/Component.hpp"
#include "mauve_runtime/Architecture.hpp"
#include "mauve_runtime/SharedData.hpp"
#include "mauve_runtime/RingBuffer.hpp"

#include <iostream>
#include <string>

using namespace mauve;

// ------------------------- Periodic Shell  -------------------------

struct PS : public Shell {
  ReadPort<time_ns_t> & period = mk_port<ReadPort<time_ns_t>>("period", sec_to_ns(1));
};

// ------------------------- Writer -------------------------

struct Archi;
struct WS: public PS {
  WritePort<std::string> & out = mk_port<WritePort<std::string>>("out");
};

struct WC: public Core<WS> {
  int count;
  Archi* archi;

  virtual void write();

  bool configure_hook() override {
    count = 0;
    return true;
  }
};

struct WF: public FiniteStateMachine<WS, WC> {
  ExecState<WC> * R;
  SynchroState<WC>      * W;

  bool configure_hook() override {
    R = mk_execution("R", &WC::write);
    W = mk_wait("W", shell().period.read());

    set_initial(R);
    set_next(R, W);
    set_next(W, R);

    return true;
  }
};

// ------------------------- Reader -------------------------

struct RS: public PS {
  ReadPort<std::string> & in = mk_port<ReadPort<std::string>>("in", "empty");
};

struct RC: public Core<RS> {
  virtual void read();
};

struct RC_reconf: public RC {
  void read() override;
};

struct RF: public FiniteStateMachine<RS, RC> {
  ExecState<RC> * R;
  SynchroState<RC>      * W;

  bool configure_hook() override {
    R = mk_execution("R", &RC::read);
    W = mk_wait("W", shell().period.read());

    set_initial(R);
    set_next(R, W);
    set_next(W, R);

    return true;
  }
};

// ------------------------- Architecture -------------------------

struct Archi : public Architecture {
  SharedData<time_ns_t> * period;
  Component<WS, WC, WF> * writer;
  Component<RS, RC, RF> * reader;
  RingBuffer<std::string> * buffer;

  bool configure_hook() override;
};

#endif
