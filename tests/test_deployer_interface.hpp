/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <iostream>
#include <stdint.h>

#include "mauve/runtime.hpp"

using namespace mauve::runtime;

struct S: public Shell   {
  Property<int>          & i          = mk_property<int>("i", 10);
  Property<uint64_t>     & j          = mk_property<uint64_t>("j", 12);
  Property<float>        & f          = mk_property<float>("f", float(3.14));
  Property<std::string>  & s          = mk_property<std::string>("s", "hello");
  EventPort              & event_port = mk_port<EventPort>("event_port");
  ReadPort<int>          & read_port  = mk_port<ReadPort<int>>("read_port", 10);
  WritePort<int>         & write_port = mk_port<WritePort<int>>("write_port");
  WritePort<std::string> & toto_port  = mk_port<WritePort<std::string>>("toto_port");
  CallPort<bool, double> & call_port  = mk_port<CallPort<bool, double>>("call_port", false);
};

struct S2: public S {};

struct C: public Core<S> {
  void update() {
    int i = shell().read_port.read();
    this->logger().info("read {}", i);
    shell().write_port.write(i+1);
  }
};
struct F: public FiniteStateMachine<S, C> {
  ExecState<C> & E = mk_execution("E", &C::update);
  SynchroState<C> & W = mk_synchronization("W", sec_to_ns(1));

  bool configure_hook() override {
    set_next(E, W);
    set_next(W, E);
    return true;
  }
};

struct Archi: public Architecture {
  Component<S, C, F> & cpt_1 = mk_component<S, C, F>("cpt_1");
  Component<S, C, F> & cpt_2 = mk_component<S, C, F>("cpt_2");
  SharedData<int> & data = mk_resource<SharedData<int>>("data", 0);

  bool configure_hook() override {
    cpt_1.replace_shell<S2>();

    cpt_1.shell().read_port.connect(data.interface().read_value);
    cpt_1.shell().write_port.connect(data.interface().write);

    cpt_1.configure();

    return true;
  }
};
