/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_CORE_HPP
#define MAUVE_RUNTIME_CORE_HPP

#include "AbstractCore.hpp"
#include "WithHook.hpp"
#include "CoreContainer.hpp"

namespace mauve {
  namespace runtime {

    class AbstractComponent;

    template <typename S>
    class HasShell;

    class AbstractLogger;

    /**
    * The Core defines the methods of the component.
    * \tparam SHELL Shell instantiated by the core.
    */
    template <typename SHELL>
    class Core
    : public AbstractCore
    , virtual public WithLogger
    , virtual public WithHook
    {

    public:
      template <typename S, typename C, typename F>
      friend class Component;
      template <typename S, typename C, typename I>
      friend class Resource;

      template <typename C>
      friend class ExecState;
      template <typename C>
      friend class SynchroState;

      /**
      * Constructor by copy
      * \param core the core to copy
      */
      Core(Core const & core) = delete;
      /**
      * Copy operator
      * \param core the core to copy
      * \return this core
      */
      Core & operator=(Core const & core) = delete;

      std::string shell_type_name() const override;

      inline std::string container_name() const override { return _container->name(); };

      inline AbstractLogger& logger() const override { return _container->logger(); }

      inline bool is_configured() const override final { return _configured; }

      inline bool configure() override final { return _container->configure_core(); }
      inline void cleanup()   override final { _container->cleanup_core(); }

    protected:
      /** Default constructor. */
      Core();
      /** Default descructor. */
      virtual ~Core() noexcept;

      /**
      * Access to the instantiated shell.
      * \return a reference to the shell instance.
      */
      inline SHELL & shell() const { return _container->shell(); };

      AbstractComponent* component() const;

    private:
      CoreContainer<SHELL>* _container;
      bool _configured;

      bool _configure();
      void _cleanup();
    };

  }
} // namespace

#include "ipp/Core.ipp"

#endif // CORE_HPP
