/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_LOGGER_IPP
#define MAUVE_RUNTIME_LOGGER_IPP

#include "mauve/runtime/logging/logger.hpp"

namespace mauve {
namespace runtime {

template <typename... Args>
void AbstractLogger::trace(const char* fmt, const Args&... args) {
  if (loggers[name()].empty() && default_logger_)
    default_logger_->trace(prepend(fmt).c_str(), args...);
  for (auto l: loggers[name()])
    l->trace(prepend(fmt).c_str(), args...);
}

template <typename... Args>
void AbstractLogger::debug(const char* fmt, const Args&... args) {
  if (loggers[name()].empty() && default_logger_)
    default_logger_->debug(prepend(fmt).c_str(), args...);
  for (auto l: loggers[name()])
    l->debug(prepend(fmt).c_str(), args...);
}

template <typename... Args>
void AbstractLogger::info(const char* fmt, const Args&... args) {
  if (loggers[name()].empty() && default_logger_)
    default_logger_->info(prepend(fmt).c_str(), args...);
  for (auto l: loggers[name()])
    l->info(prepend(fmt).c_str(), args...);
}

template <typename... Args>
void AbstractLogger::warn(const char* fmt, const Args&... args) {
  if (loggers[name()].empty() && default_logger_)
    default_logger_->warn(prepend(fmt).c_str(), args...);
  for (auto l: loggers[name()])
    l->warn(prepend(fmt).c_str(), args...);
}

template <typename... Args>
void AbstractLogger::error(const char* fmt, const Args&... args) {
  if (loggers[name()].empty() && default_logger_)
    default_logger_->error(prepend(fmt).c_str(), args...);
  for (auto l: loggers[name()])
    l->error(prepend(fmt).c_str(), args...);
}

template <typename... Args>
void AbstractLogger::critical(const char* fmt, const Args&... args) {
  if (loggers[name()].empty() && default_logger_)
    default_logger_->critical(prepend(fmt).c_str(), args...);
  for (auto l: loggers[name()])
    l->critical(prepend(fmt).c_str(), args...);
}

}} // namespace

#endif
