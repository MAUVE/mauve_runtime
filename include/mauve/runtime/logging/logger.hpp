/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_LOGGER_HPP
#define MAUVE_RUNTIME_LOGGER_HPP

#include <iostream>

#include "spdlog/spdlog.h"
#include "spdlog/fmt/bundled/ostream.h"

namespace YAML {
  class Node;
}

namespace mauve {
  namespace runtime {

  class AbstractComponent;
  class AbstractResource;

  /**
   * Class of a generic MAUVE logger
   */
  class AbstractLogger {
  public:
    /** Constructor */
    AbstractLogger();

    /** Destructor **/
    virtual ~AbstractLogger();

    /**
     * Initialize the logging framework with default parameters.
     */
    static void initialize();

    /**
     * Initialize the logging framework from an input stream.
     * \param is the input stream containing formatted parameters
     */
    static void initialize(std::istream& is);

    /**
     * Initialize the logging framework from a string.
     * \param s the input string containing formatted parameters
     */
    static void initialize(const std::string& s);

    /**
     * Initialize the logging framework from a YAML node.
     * \param is the input YAML configuration
     */
    static void initialize(const YAML::Node& n);

    /**
     * Clear all loggers information.
     */
    static void clear();

    /**
     * Get a logger by its name
     * \param name the logger name
     * \return a pointer to the logger with name \a name,
     * or to a null logger if unregistered
     */
    static AbstractLogger* get_logger(const std::string& name);

    /**
     * Log a message at TRACE level.
     * \tparam Args message arguments type
     * \param fmt formatted message
     * \param args message arguments
     */
    template <typename... Args>
    void trace(const char* fmt, const Args&... args);

      /**
       * Log a message at DEBUG level.
       * \tparam Args message arguments type
       * \param fmt formatted message
       * \param args message arguments
       */
    template <typename... Args>
    void debug(const char* fmt, const Args&... args);

    /**
     * Log a message at INFO level.
     * \tparam Args message arguments type
     * \param fmt formatted message
     * \param args message arguments
     */
    template <typename... Args>
    void info(const char* fmt, const Args&... args);

    /**
     * Log a message at WARN level.
     * \tparam Args message arguments type
     * \param fmt formatted message
     * \param args message arguments
     */
    template <typename... Args>
    void warn(const char* fmt, const Args&... args);

    /**
     * Log a message at ERROR level.
     * \tparam Args message arguments type
     * \param fmt formatted message
     * \param args message arguments
     */
    template <typename... Args>
    void error(const char* fmt, const Args&... args);

    /**
     * Log a message at CRITICAL level.
     * \tparam Args message arguments type
     * \param fmt formatted message
     * \param args message arguments
     */
    template <typename... Args>
    void critical(const char* fmt, const Args&... args);

  protected:
    /**
     * Store spdlog loggers associated to the categories
     */
    static std::map<std::string,
      std::vector< std::shared_ptr<spdlog::logger> > > loggers;
    /**
     * Default logger when no category specified
     */
    static std::shared_ptr<spdlog::logger> default_logger_;
    /**
     * Prepend log message with contextual data
     * \param fmt the original message
     * \return a new message with component data
     */
    virtual std::string prepend(const char* fmt) = 0;
    /**
     * Get Logger category name.
     * \return the category name attached to this logger
     */
    virtual std::string name() = 0;
    /**
     * Convert a string to a log level.
     * \param lvl a string notation of the level
     * \return a spdlog level.
     */
    static spdlog::level::level_enum level_enum(const std::string& lvl);

  };

  /**
   * Class of a MAUVE logger for a custom category
   */
  class CategoryLogger final : public AbstractLogger {
  public:
    /**
     * Create a new logger
     * \param name the logger name
     * \return a pointer to the new logger, or to an existing logger if already registered
     */
    CategoryLogger(const std::string& name);
  protected:
    std::string prepend(const char* fmt) override;
    std::string name() override;
  private:
    std::string name_;
  };

  /**
   * Class of a MAUVE logger for a component
   */
  class ComponentLogger final : public AbstractLogger {
  public:
    /**
     * Create a logger for a component
     * \param comp the component that logs
     */
    ComponentLogger(AbstractComponent* component);
  protected:
    std::string prepend(const char* fmt) override;
    std::string name() override;
  private:
    /**
     * Pointer to the component owning the logger.
     */
    AbstractComponent* component_;
    std::string status();
  };

  /**
   * Class of a MAUVE logger for a resrouce
   */
  class ResourceLogger final : public AbstractLogger {
  public:
    /**
     * Create a logger for a resource
     * \param comp the resour that logs
     */
    ResourceLogger(AbstractResource* resource);
  protected:
    std::string prepend(const char* fmt) override;
    std::string name() override;
  private:
    /**
     * Pointer to the component owning the logger.
     */
    AbstractResource* resource_;
    std::string status();
  };

  /**
   * Class of a MAUVE logger for the deployer
   */
  class DeployerLogger final : public AbstractLogger {
  public:
    DeployerLogger();
  protected:
    std::string name() override;
    std::string prepend(const char* fmt) override;

  };

}}

#include "ipp/logger.ipp"

#endif
