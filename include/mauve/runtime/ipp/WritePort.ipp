/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WRITE_PORT_IPP
#define MAUVE_RUNTIME_WRITE_PORT_IPP

#include "mauve/runtime/WritePort.hpp"
#include "mauve/runtime/WriteService.hpp"

namespace mauve {
  namespace runtime {

  // -------------------- Constructor --------------------

  template <typename T>
  WritePort<T>::WritePort(HasPort* container, std::string const & name)
  : Port<WriteService<T>> { container, name }
  {}

  // -------------------- Destructor --------------------

  template <typename T>
  WritePort<T>::~WritePort() noexcept {}

  // -------------------- Method --------------------

  template <typename T>
  std::string WritePort<T>::type_name() const {
    int status = -4;
    std::string type_name = abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
    return type_name;
  }

  template <typename T>
  void WritePort<T>::write(T value) const {
    for (WriteService<T>* service: this->services) {
      service->write(value);
    }
  }

}} /* namespace mauve */

#endif
