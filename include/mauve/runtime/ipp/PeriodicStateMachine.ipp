/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_PERIODICSTATEMACHINE_IPP
#define MAUVE_RUNTIME_PERIODICSTATEMACHINE_IPP

#include "../PeriodicStateMachine.hpp"

namespace mauve {
  namespace runtime {

	template <typename SHELL, typename CORE>
	bool PeriodicStateMachine<SHELL, CORE>::configure_hook() {
    this->sync_state.set_clock(period);
    this->set_initial(exec_state);
    this->set_next(exec_state, sync_state);
    this->set_next(sync_state, exec_state);
    return true;
  }

  }
}

#endif // MAUVE_RUNTIME_PERIODICSTATEMACHINE_IPP
