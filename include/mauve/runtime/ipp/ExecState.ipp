/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_EXEC_STATE_IPP
#define MAUVE_RUNTIME_EXEC_STATE_IPP

#include "mauve/runtime/ExecState.hpp"
#include "mauve/runtime/Transition.hpp"
#include "mauve/runtime/FiniteStateMachine.hpp"
#include "mauve/runtime/AbstractFiniteStateMachine.hpp"

#include <iostream>

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  template <typename CORE>
  ExecState<CORE>::ExecState(AbstractFiniteStateMachine* container, std::string const & name, Update_t update)
  : State<CORE> { container, name }
  , update      { update }
  {}

  // ------------------------- Destructor -------------------------

  template <typename CORE>
  ExecState<CORE>::~ExecState() noexcept {
    for (Transition<CORE> * transition : transitions) {
      delete transition;
    }
  }

  // ------------------------- Operator -------------------------

  // ------------------------- Method -------------------------

  template <typename CORE>
  void ExecState<CORE>::mk_transition(Guard_t guard, State<CORE> & next) {
    Transition<CORE> * transition = new Transition<CORE>(guard, next);
    transitions.push_back(transition);
  }

  template <typename CORE>
  bool ExecState<CORE>::is_execution() const {
    return true;
  }

  template <typename CORE>
  bool ExecState<CORE>::is_synchronization() const {
    return false;
  }

  template <typename CORE>
  time_ns_t ExecState<CORE>::get_clock() const {
    return 0;
  }

  template <typename CORE>
  bool ExecState<CORE>::set_clock(time_ns_t clock) {
    return false;
  }

  template <typename CORE>
  bool ExecState<CORE>::set_update(Update_t update) {
    if (this->container->is_configured()) {
      return false;
    }
    this->update = update;
    return true;
  }

  template <typename CORE>
  AbstractState* ExecState<CORE>::get_next_state(int index) const {
    const auto& nexts = this->next_states();
    if (index < 0 || (unsigned)index >= nexts.size()) return nullptr;
    return nexts[(unsigned)index];
  }

  template <typename CORE>
  std::vector<State<CORE>*> ExecState<CORE>::next_states() const {
    std::vector<State<CORE>*> res;
    for (Transition<CORE>* transition: transitions) {
      res.push_back(&(transition->next));
    }
    res.push_back(this->next);
    return res;
  }

  template <typename CORE>
  State<CORE> * ExecState<CORE>::run(CORE * core) {
    core->logger().trace("running state {}", this->to_string());
    update(core);
    for (Transition<CORE> * t : transitions) {
      core->logger().trace("testing transition to {}", t->next.to_string());
      if (t->check(core)) {
        core->logger().trace("go to state {}", t->next.to_string());
        return &(t->next);
      }
    }
    core->logger().trace("go to state {}", this->next->to_string());
    return this->next;
  }

  template <typename CORE>
  std::string ExecState<CORE>::to_string() const {
    return "ExecState[" + this->name + "]";
  }

}}

#endif
