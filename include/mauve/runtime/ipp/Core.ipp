/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_CORE_IPP
#define MAUVE_RUNTIME_CORE_IPP

#include "mauve/runtime/Core.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/FiniteStateMachine.hpp"

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    template <typename SHELL>
    Core<SHELL>::Core()
    : AbstractCore {}
    , _container   { nullptr }
    , _configured  { false }
    {
    }

    // ------------------------- Destructor -------------------------

    template <typename SHELL>
    Core<SHELL>::~Core() noexcept {
      _cleanup();
    }

    // ------------------------- Operator -------------------------

    // ------------------------- Method -------------------------

    template <typename SHELL>
    std::string Core<SHELL>::shell_type_name() const {
      int status = -4; // some arbitrary value to eliminate the compiler warning
      return abi::__cxa_demangle(typeid(SHELL).name(), nullptr, nullptr, &status);
    }

    template <typename SHELL>
    AbstractComponent* Core<SHELL>::component() const {
      return dynamic_cast<AbstractComponent*>(_container);
    }

    template <typename SHELL>
    bool Core<SHELL>::_configure() {
      if (is_configured()) return true;
      _configured = configure_hook();
      return _configured;
    }

    template <typename SHELL>
    void Core<SHELL>::_cleanup() {
      if (is_configured()) {
        cleanup_hook();
        _configured = false;
      }
    }

  }
}

#endif
