/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_RESOURCE_IPP
#define MAUVE_RUNTIME_RESOURCE_IPP

#include "mauve/runtime/Resource.hpp"

namespace mauve {
  namespace runtime {

    // -------------------- Constructor --------------------

    template <typename SHELL, typename CORE, typename INTERFACE>
    Resource<SHELL, CORE, INTERFACE>::Resource(std::string const & name)
    : AbstractResource { name }
    , _logger          { new ResourceLogger(this) }
    , _shell           { nullptr }
    , _core            { nullptr }
    , _interface       { nullptr }
    {
    }

    // -------------------- Destructor --------------------

    template <typename SHELL, typename CORE, typename INTERFACE>
    Resource<SHELL, CORE, INTERFACE>::~Resource() noexcept {
      delete _logger;
      cleanup();
      clear();
    }

    // -------------------- Method --------------------

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::is_empty() const {
      return (_shell == nullptr) && (_core == nullptr) && (_interface == nullptr);
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::is_configured() const {
      if ((_shell == nullptr) || (_core == nullptr) || (_interface == nullptr)) return false;
      return _shell->is_configured() && _core->is_configured() && _interface->is_configured();
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::configure() {
      if (!configure_shell())     return false;
      if (!configure_core())      return false;
      if (!configure_interface()) return false;
      DeployerLogger().info("Resource {} configured", name());
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    void Resource<SHELL, CORE, INTERFACE>::cleanup() {
      cleanup_interface();
      cleanup_core();
      cleanup_shell();
      DeployerLogger().info("Resource {} cleaned up", name());
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::clear() {
      if (!clear_interface()) return false;
      if (!clear_core())      return false;
      if (!clear_shell())     return false;
      DeployerLogger().info("Resource {} cleared", name());
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make(P... parameters) {
      return make<SHELL, CORE, INTERFACE>(parameters...);
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename S, typename C, typename I, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make(P... parameters) {
      if (!is_empty())                   return false;
      if (!make_shell<S>(parameters...)) return false;
      if (!make_core<C>())               return false;
      if (!make_interface<I>())          return false;
      DeployerLogger().info("Resource {} made", name());
      return true;
    }

    // ---------- Shell ----------

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::configure_shell() {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core == nullptr)        return false;
      if (_core->is_configured())  return false;
      DeployerLogger().debug("Configuring shell {} of resource {}", _shell->type_name(), name());
      return _shell->_configure();
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::cleanup_shell() {
      if (_shell == nullptr)        return false;
      if (!_shell->is_configured()) return false;
      if (_core == nullptr)         return false;
      if (_core->is_configured())   return false;
      DeployerLogger().debug("Cleaning up shell {} of resource {}", _shell->type_name(), name());
      _shell->_cleanup();
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::clear_shell() {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core != nullptr)        return false;
      DeployerLogger().debug("Clearing shell {} of resource {}", _shell->type_name(), name());
      delete _shell;
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make_shell(P... parameters) {
      return make_shell<SHELL>(parameters...);
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename S, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make_shell(P... parameters) {
      if (_shell != nullptr) return false;
      if (_core != nullptr)  return false;
      _shell = new S(parameters...);
      _shell->_container = this;
      DeployerLogger().debug("Made shell {} of resource {}", _shell->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename S, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::replace_shell(P... parameters) {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core == nullptr)        return false;
      if (_core->is_configured())  return false;
      DeployerLogger().debug("Replacing shell {} of resource {}", _shell->type_name(), name());
      delete _shell;
      _shell = new S(parameters...);
      _shell->_container = this;
      return true;
    }

    // ---------- Core ----------

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::configure_core() {
      if (_shell == nullptr)           return false;
      if (!_shell->is_configured())    return false;
      if (_core == nullptr)            return false;
      if (_core->is_configured())      return false;
      if (_interface == nullptr)       return false;
      if (_interface->is_configured()) return false;
      DeployerLogger().debug("Configuring core {} of resource {}", _core->type_name(), name());
      return _core->_configure();
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::cleanup_core() {
      if (_shell == nullptr)           return false;
      if (!_shell->is_configured())    return false;
      if (_core == nullptr)            return false;
      if (!_core->is_configured())     return false;
      if (_interface == nullptr)       return false;
      if (_interface->is_configured()) return false;
      DeployerLogger().debug("Cleaning up core {} of resource {}", _core->type_name(), name());
      _core->_cleanup();
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::clear_core() {
      if (_shell == nullptr)       return false;
      if (_shell->is_configured()) return false;
      if (_core == nullptr)        return false;
      if (_core->is_configured())  return false;
      if (_interface != nullptr)   return false;
      DeployerLogger().debug("Clearing core {} of resource {}", _core->type_name(), name());
      delete _core;
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make_core(P... parameters) {
      return make_core<CORE>(parameters...);
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename C, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make_core(P... parameters) {
      if (_shell == nullptr)     return false;
      if (_core != nullptr)      return false;
      if (_interface != nullptr) return false;
      _core = new C(parameters...);
      _core->_container = this;
      DeployerLogger().debug("Made core {} of resource {}", _core->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename C, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::replace_core(P... parameters) {
      if (_shell == nullptr)           return false;
      if (_core == nullptr)            return false;
      if (_core->is_configured())      return false;
      if (_interface == nullptr)       return false;
      if (_interface->is_configured()) return false;
      DeployerLogger().debug("Replacing core {} of resource {}", _core->type_name(), name());
      delete _core;
      _core = new C(parameters...);
      _core->_container = this;
      return true;
    }

    // ---------- Interface ----------

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::configure_interface() {
      if (_core == nullptr)            return false;
      if (!_core->is_configured())     return false;
      if (_interface == nullptr)       return false;
      if (_interface->is_configured()) return false;
      DeployerLogger().debug("Configuring interface {} of resource {}", _interface->type_name(), name());
      return _interface->_configure();
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::cleanup_interface() {
      if (_core == nullptr)             return false;
      if (!_core->is_configured())      return false;
      if (_interface == nullptr)        return false;
      if (!_interface->is_configured()) return false;
      DeployerLogger().debug("Cleaning up interface {} of resource {}", _interface->type_name(), name());
      _interface->_cleanup();
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    bool Resource<SHELL, CORE, INTERFACE>::clear_interface() {
      if (_core == nullptr)            return false;
      if (_core->is_configured())      return false;
      if (_interface == nullptr)       return false;
      if (_interface->is_configured()) return false;
      DeployerLogger().debug("Clearing interface {} of resource {}", _interface->type_name(), name());
      delete _interface;
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make_interface(P... parameters) {
      return make_interface<INTERFACE>(parameters...);
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename I, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::make_interface(P... parameters) {
      if (_core == nullptr)      return false;
      if (_interface != nullptr) return false;
      _interface = new I(parameters...);
      _interface->_container = this;
      DeployerLogger().debug("Made interface {} of resource {}", _interface->type_name(), name());
      return true;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    template <typename I, typename ...P>
    bool Resource<SHELL, CORE, INTERFACE>::replace_interface(P... parameters) {
      if (_core == nullptr)           return false;
      if (_interface == nullptr)            return false;
      if (_interface->is_configured())      return false;
      DeployerLogger().debug("Replacing interface {} of resource {}", _interface->type_name(), name());
      delete _interface;
      _interface = new I(parameters...);
      _interface->_container = this;
      return true;
    }

  }
} /* namespace mauve */

#endif
