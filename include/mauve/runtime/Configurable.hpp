/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_CONFIGURABLE_HPP
#define MAUVE_RUNTIME_CONFIGURABLE_HPP

namespace mauve {
  namespace runtime {

    /** Configurable trait */
    struct Configurable {
      /**
       * Get the configuration status of the configurable object.
       * \return true if the object is configured
       */
      virtual bool is_configured() const = 0;
      /** Configure the object */
      virtual bool configure() = 0;
      /** Cleanup the object */
      virtual void cleanup  () = 0;
    };
  }
}
#endif
