/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WITH_ABSTRACT_FSM_HPP
#define MAUVE_RUNTIME_WITH_ABSTRACT_FSM_HPP

namespace mauve {
  namespace runtime {

    class AbstractFiniteStateMachine;

    /** Trait for objects with an abstract FSM */
    struct WithAbstractFSM {
      /** Get a pointer to the FSM */
      virtual AbstractFiniteStateMachine* get_fsm () const = 0;
      /** Check if the FSM is empty */
      virtual bool is_empty_fsm                   () const = 0;
      /** Configure the FSM */
      virtual bool configure_fsm                  ()       = 0;
      /** Clean up the FSM */
      virtual bool cleanup_fsm                    ()       = 0;
    };
  }
}

#endif
