/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_RESOURCE_HPP
#define MAUVE_RUNTIME_RESOURCE_HPP

#include "AbstractResource.hpp"
#include "WithLogger.hpp"
#include "WithShell.hpp"
#include "WithCore.hpp"
#include "WithInterface.hpp"
#include "ShellContainer.hpp"
#include "CoreContainer.hpp"
#include "InterfaceContainer.hpp"

namespace mauve {
  namespace runtime {

    class Shell;
    class AbstractCore;
    class AbstractInterface;

    template <typename SHELL, typename CORE, typename INTERFACE>
    class Resource final
    : public AbstractResource
    , virtual public WithLogger
    , virtual public WithShell<SHELL>
    , virtual public WithCore<CORE>
    , virtual public WithInterface<INTERFACE>
    , virtual public ShellContainer
    , virtual public CoreContainer<SHELL>
    , virtual public InterfaceContainer<SHELL, CORE>
    {
    public:
      Resource(std::string const & name);
      virtual ~Resource() noexcept;

      Resource() = delete;
      Resource(Resource const & other) = delete;
      Resource & operator=(Resource const & other) = delete;

      // ---------- Logger----------

      inline AbstractLogger & logger() const override { return *_logger; }

      // ---------- Resource ----------

      bool is_empty     () const override;
      bool is_configured() const override;

      bool configure() override;
      void cleanup  () override;

      bool clear() override;

      template <typename ...P>
      bool make(P... parameters);

      template <typename S, typename C, typename I, typename ...P>
      bool make(P... parameters);

      // ---------- Shell ----------

      inline Shell * get_shell() const override { return _shell; }
      inline SHELL & shell() const override { return *_shell; }

      inline bool is_empty_shell() const override { return _shell == nullptr; }

      bool configure_shell() override;
      bool cleanup_shell  () override;

      bool clear_shell() override;

      template <typename ...P>
      bool make_shell(P... parameters);

      template <typename S, typename ...P>
      bool make_shell(P... parameters);

      template <typename S, typename ...P>
      bool replace_shell(P... parameters);

      // ---------- Core ----------

      inline AbstractCore * get_core() const override { return _core; }
      inline CORE & core() const override { return *_core; }
      inline CORE * core_ptr() const override { return _core; }

      inline bool is_empty_core() const override { return _core == nullptr; }

      bool configure_core() override;
      bool cleanup_core  () override;

      bool clear_core() override;

      template <typename ...P>
      bool make_core(P... parameters);

      template <typename C, typename ...P>
      bool make_core(P... parameters);

      template <typename C, typename ...P>
      bool replace_core(P... parameters);

      // ---------- Interface ----------

      inline AbstractInterface * get_interface() const override { return _interface; }
      inline INTERFACE & interface() const override { return *_interface; }

      inline bool is_empty_interface() const override { return _interface == nullptr; }

      bool configure_interface() override;
      bool cleanup_interface  () override;

      bool clear_interface() override;

      template <typename ...P>
      bool make_interface(P... parameters);

      template <typename I, typename ...P>
      bool make_interface(P... parameters);

      template <typename I, typename ...P>
      bool replace_interface(P... parameters);

      int get_service_index(const Service* service) const override { return _interface->get_service_index(service); }

    private:
      ResourceLogger* _logger;
      SHELL*          _shell;
      CORE*           _core;
      INTERFACE*      _interface;
    };

  }
} /* namespace mauve */

#include "ipp/Resource.ipp"

#endif
