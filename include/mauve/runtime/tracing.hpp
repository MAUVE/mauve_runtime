/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#ifndef MAUVE_RUNTIME_TRACING_HPP
#define MAUVE_RUNTIME_TRACING_HPP

#include <string>

namespace mauve {
  namespace runtime {

    class AbstractComponent;
    class AbstractState;
    class AbstractResource;
    class Service;

    void trace_component_start(AbstractComponent* c);

    void trace_execution_begin(AbstractComponent* c, AbstractState* s);

    void trace_execution_end(AbstractComponent* c, AbstractState* s);

    void trace_synchronization_begin(AbstractComponent* c, AbstractState* s);

    void trace_synchronization_end(AbstractComponent* c, AbstractState* s);

    void trace_service_begin(const std::string& resource_name, const std::string& service_name);

    void trace_service_end(const std::string& resource_name, const std::string& service_name);

  }
}

#endif
