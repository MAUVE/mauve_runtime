/*
* Copyright 2017 ONERA
*
* This file is part of the MAUVE Runtime project.
*
* MAUVE Runtime is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3 as
* published by the Free Software Foundation.
*
* MAUVE Runtime is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
*/
#ifndef MAUVE_RUNTIME_CALL_SERVICE_HPP
#define MAUVE_RUNTIME_CALL_SERVICE_HPP

#include <functional>

#include "Service.hpp"

namespace mauve {
  namespace runtime {

    template <typename C>
    struct ServiceContainer;

    template <typename R, typename ...P>
    class CallPort;

    /** Class of a Call service */
    template <typename R, typename ...P>
    class CallService: public Service {
    public:
      /** Constructor */
      CallService(std::string const & name): Service{name} {}
      virtual ~CallService() noexcept {}

      CallService() = delete;
      CallService(const CallService<R, P...> & other) = delete;

      connection_type get_type() const override final { return CALL; }

      /** Call the service */
      virtual R call(P... parameters) const = 0;
      /** Helper */
      inline R operator()(P... parameters) const { return call(parameters ...); };
    };

    /**
     * Call Service implementation
     * \tparam CORE Core of the resource
     * \tparam R Return type
     * \tparam P Parameters types
     */
    template <typename CORE, typename R, typename ...P>
    class CallServiceImpl final: public CallService<R, P...> {
    public:
      /** Service action type */
      using action_t = std::function<R(CORE*, P...)>;

      /** Constructor */
      CallServiceImpl(ServiceContainer<CORE> * container, std::string const & name, action_t action);
      ~CallServiceImpl() noexcept;

      CallServiceImpl() = delete;
      /** Constructor */
      CallServiceImpl(const CallService<CORE, R, P...> & other) = delete;

      std::string get_resource_name() const override { return container->name(); }

      /** Call the service */
      R call(P... parameters) const override;

    private:
      ServiceContainer<CORE> * container;
      action_t action;
    };

  }
} // namespaces

#include "ipp/CallService.ipp"

#endif
