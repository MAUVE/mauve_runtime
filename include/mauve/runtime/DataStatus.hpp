/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_DATA_STATUS_HPP
#define MAUVE_RUNTIME_DATA_STATUS_HPP

#include <iostream>

namespace mauve {
  namespace runtime {

    enum class DataStatus { NO_DATA, NEW_DATA, OLD_DATA };

    template <typename T>
    struct StatusValue {
      DataStatus status;
      T value;

      StatusValue(DataStatus status, T value): status{status}, value{value} {};
      StatusValue(T value): status{DataStatus::NO_DATA}, value{value} {};
      StatusValue(): status{DataStatus::NO_DATA}, value{T()} {};
    };

    std::ostream & operator<<(std::ostream & out, DataStatus status);

    template <typename T>
    inline std::ostream & operator<<(std::ostream & out, StatusValue<T> status_value) {
      out << status_value.value << " (" << status_value.status << ")";
      return out;
    };
  }
}

#endif
