/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_INTERFACE_HPP
#define MAUVE_RUNTIME_INTERFACE_HPP

#include "AbstractInterface.hpp"
#include "ServiceContainer.hpp"
#include "EventService.hpp"
#include "ReadService.hpp"
#include "WriteService.hpp"
#include "CallService.hpp"
#include "InterfaceContainer.hpp"
#include "WithHook.hpp"

namespace mauve {
  namespace runtime {

    template <typename SHELL, typename CORE>
    class Interface
    : public AbstractInterface
    , virtual public ServiceContainer<CORE>
    , virtual public WithLogger
    , virtual public WithHook
    {
    public:
      template <typename S, typename C, typename I>
      friend class Resource;

      Interface(Interface const & other) = delete;

      std::string name() const override { return _container->name(); }

      // ---------- Service ----------

      std::size_t get_services_size() const override final { return services.size(); }

      const std::vector<Service*> get_services() const override final;

      Service* get_service(std::string const & name) const override final;

      Service* get_service(int index) const override final;

      int get_service_index(const Service* service) const override final;

      inline AbstractLogger& logger() const override { return _container->logger(); };

      inline bool is_configured() const override final { return _configured; }

      inline bool configure() override final { return _container->configure_interface(); }
      inline void cleanup()   override final { _container->cleanup_interface(); }

      inline CORE * core_ptr () const override final { return _container->core_ptr(); }

    protected:
      Interface();
      virtual ~Interface() noexcept;

      inline SHELL & shell() const { return _container->shell(); };
      inline CORE  & core () const { return _container->core(); };

      // ---------- Service ----------

      EventService &
      mk_event_service(std::string const & name, typename EventServiceImpl<CORE>::action_t action);

      template <typename T>
      ReadService<T> &
      mk_read_service(std::string const & name, typename ReadServiceImpl<CORE, T>::action_t action);

      template <typename T>
      WriteService<T> &
      mk_write_service(std::string const & name, typename WriteServiceImpl<CORE, T>::action_t action);

      template <typename R, typename ...P>
      CallService<R, P...> &
      mk_call_service(std::string const & name, typename CallServiceImpl<CORE, R, P...>::action_t action);

    private:
      InterfaceContainer<SHELL, CORE> *_container;
      bool _configured;
      std::vector<Service *> services;

      bool _configure();
      void _cleanup();
    };

    // -------------------- Exceptions --------------------

    /** Exception for Already Defined Services */
    struct AllreadyDefinedService: public std::exception {
      /** Constructor */
      AllreadyDefinedService(std::string const & name) throw() : name(name) {}
      /** Exception name */
      const std::string name;
      /** Exception explanation */
      virtual const char* what() const throw() {
        std::string message = "Allready Defined Service " + name;
        return message.c_str();
      }
    };

  }
}

#include "ipp/Interface.ipp"

#endif
