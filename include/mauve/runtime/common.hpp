/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_COMMON_HPP
#define MAUVE_RUNTIME_COMMON_HPP

#include <iostream>
#include <signal.h>
#include <stdint.h>
#include <time.h>

#define MAUVE_CLOCK_ID CLOCK_MONOTONIC

#define sigev_notify_thread_id _sigev_un._tid

namespace mauve {
  namespace runtime {

  // ------------------------- Time -------------------------

  using time_ns_t = uint64_t;

  constexpr time_ns_t timespec_to_ns(const struct timespec & ts) {
    return (time_ns_t)ts.tv_sec * 1000000000 + (time_ns_t)ts.tv_nsec;
  }

  constexpr time_ns_t timeval_to_ns(const struct timeval & ts) {
    return (time_ns_t)ts.tv_sec * 1000000000 + (time_ns_t)ts.tv_usec * 1000;
  }

  inline void ns_to_timespec(time_ns_t ns, struct timespec* ts) {
    ts->tv_sec  = ns / 1000000000;
    ts->tv_nsec = ns % 1000000000;
  }

  inline void ns_to_itimerspec(time_ns_t period, time_ns_t value, struct itimerspec* its) {
    its->it_interval.tv_sec  = period / 1000000000;
    its->it_interval.tv_nsec = period % 1000000000;
    its->it_value.tv_sec     = value / 1000000000;
    its->it_value.tv_nsec    = value % 1000000000;
  }

  constexpr time_ns_t ms_to_ns(time_ns_t ms) {
    return ms * 1000000;
  }

  constexpr time_ns_t sec_to_ms(time_ns_t sec) {
    return sec * 1000;
  }

  constexpr time_ns_t sec_to_us(time_ns_t sec) {
    return sec * 1000000;
  }

  constexpr time_ns_t sec_to_ns(time_ns_t sec) {
    return sec * 1000000000;
  }

  constexpr time_ns_t ns_to_sec(time_ns_t ns) {
    return ns / 1000000000;
  }

  inline std::ostream & operator<<(std::ostream & out, struct timespec const & ts) {
    out << "struct timespec {\n";
    out << "\ttime_t tv_sec  = " << ts.tv_sec << std::endl;
    out << "\tlong   tv_nsec = " << ts.tv_nsec << std::endl;
    out << "}\n";
    return out;
  }

  inline std::ostream & operator<<(std::ostream & out, struct itimerspec const & its) {
    out << "struct itimerspec {\n";
    out << "\tstruct timespec it_interval  = " << its.it_interval.tv_sec << " sec " << its.it_interval.tv_nsec << " ns \n";
    out << "\tstruct timespec it_value     = " << its.it_value.tv_sec << " sec " << its.it_value.tv_nsec << " ns \n";
    out << "}\n";
    return out;
  }

  // ------------------------- Task -------------------------

  using task_id_t = int32_t;

  // ------------------------- Version -------------------------

  using version_t = uint32_t;

}} // namespaces

#endif
