/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_COMPONENT_HPP
#define MAUVE_RUNTIME_COMPONENT_HPP

#include "common.hpp"
#include "AbstractComponent.hpp"
#include "WithLogger.hpp"
#include "WithShell.hpp"
#include "WithCore.hpp"
#include "WithFSM.hpp"
#include "ShellContainer.hpp"
#include "CoreContainer.hpp"
#include "FSMContainer.hpp"

#include <string>
#include <iostream>
#include <pthread.h>

#define MIN_PRIO  1
#define MAX_PRIO 99

namespace mauve {
  namespace runtime {

    class Architecture;
    class Shell;
    class AbstractCore;
    class AbstractFiniteStateMachine;
    template <typename C>
    class State;

    /** Component class. A component is made of a Shell, a Core, and a FiniteStateMachine
     * \tparam SHELL Shell type
     * \tparam CORE Core type
     * \tparam FSM FiniteStateMachine type
     */
    template <typename SHELL, typename CORE, typename FSM>
    class Component final
    : public AbstractComponent
    , virtual public Configurable
    , virtual public WithShell<SHELL>
    , virtual public WithCore<CORE>
    , virtual public WithFSM<FSM>
    , virtual public ShellContainer
    , virtual public CoreContainer<SHELL>
    , virtual public FSMContainer<SHELL, CORE>
    {
    public:
      friend class Architecture;

      Component(Component const & component) = delete;
      Component * operator&() = delete;

      // ---------- Logger----------

      inline AbstractLogger & logger() const override { return *_logger; }

      // ---------- Component----------

      bool is_empty     () const override;
      bool is_configured() const override;
      bool is_created   () const override;
      bool is_activated () const override;
      bool is_running   () const override;

      bool configure() override;
      void cleanup  () override;

      void disconnect() override;

      bool clear() override;

      template <typename ...P>
      bool make(P... parameters);

      template <typename S, typename C, typename F, typename ...P>
      bool make(P... parameters);

      /** Get the task executing this component */
      inline Task & get_task() const { return *_task; }
      void set_task(Task* task) override;

      time_ns_t get_time() const override;

      inline int get_priority() const override { return _priority; }
      bool set_priority(int priority) override;

      inline int get_cpu() const override { return _cpu; }
      bool set_cpu(int cpu) override;

      // ---------- Shell ----------

      inline Shell * get_shell() const override { return _shell; }
      inline SHELL & shell() const override { return *_shell; }

      inline bool is_empty_shell() const override { return _shell == nullptr; }

      bool configure_shell() override;
      bool cleanup_shell  () override;

      bool clear_shell() override;

      template <typename ...P>
      bool make_shell(P... parameters);

      template <typename S, typename ...P>
      bool make_shell(P... parameters);

      template <typename S, typename ...P>
      bool replace_shell(P... parameters);

      // ---------- Core ----------

      inline AbstractCore * get_core() const override { return _core; }
      inline CORE & core() const override { return *_core; }
      inline CORE * core_ptr () const override final { return _core; }

      inline bool is_empty_core() const override { return _core == nullptr; }

      bool configure_core() override;
      bool cleanup_core  () override;

      bool clear_core() override;

      template <typename ...P>
      bool make_core(P... parameters);

      template <typename C, typename ...P>
      bool make_core(P... parameters);

      template <typename C, typename ...P>
      bool replace_core(P... parameters);

      // ---------- Finite State Machine ----------

      inline AbstractFiniteStateMachine * get_fsm() const override { return _fsm; }
      inline FSM & fsm() const override { return *_fsm; }

      inline bool is_empty_fsm() const override { return _fsm == nullptr; }

      bool configure_fsm() override;
      bool cleanup_fsm  () override;

      bool clear_fsm() override;

      template <typename ...P>
      bool make_fsm(P... parameters);

      template <typename F, typename ...P>
      bool make_fsm(P... parameters);

      template <typename F, typename ...P>
      bool replace_fsm(P... parameters);

      bool step() override;

      AbstractState * current_state() override;

    private:
      Component(std::string const & name);
      ~Component() noexcept;

      bool run() override;

      ComponentLogger* _logger;
      SHELL* _shell;
      CORE*  _core;
      FSM*   _fsm;
      State<CORE>* current;
      Task* _task;
      int _priority;
      int _cpu;
    };

  }} // namespaces

  #include "ipp/Component.ipp"

  #endif // MAUVE_RUNTIME_COMPONENT_HPP
