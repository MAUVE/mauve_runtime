/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_READ_SERVICE_HPP
#define MAUVE_RUNTIME_READ_SERVICE_HPP

#include <functional>

#include "Service.hpp"

namespace mauve {
  namespace runtime {

    template <typename C>
    struct ServiceContainer;

    template <typename T>
    class ReadService : public Service {
    public:
      ReadService(std::string const & name): Service{name} {}
      virtual ~ReadService() noexcept {}

      ReadService() = delete;
      ReadService(const ReadService<T> & other) = delete;

      connection_type get_type() const override final { return READ; }

      virtual T read() const = 0;
      inline T operator()() const { return read(); }
    };


    template <typename CORE, typename T>
    class ReadServiceImpl final : public ReadService<T> {
    public:
      using action_t = std::function<T(CORE*)>;

      ReadServiceImpl(ServiceContainer<CORE> * container, std::string const & name, action_t action);
      ~ReadServiceImpl() noexcept;

      ReadServiceImpl() = delete;
      ReadServiceImpl(const ReadServiceImpl<CORE, T> & other) = delete;

      std::string get_resource_name() const override { return container->name(); }

      T read() const override;

    private:
      ServiceContainer<CORE> * container;
      action_t action;
    };

  }
} /* namespace mauve */

#include "ipp/ReadService.ipp"

#endif
