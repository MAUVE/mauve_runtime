/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_READ_PORT_HPP
#define MAUVE_RUNTIME_READ_PORT_HPP

#include "Port.hpp"

namespace mauve {
  namespace runtime {

    template <typename T>
    class ReadService;

    template <typename T>
    class ReadPort final : public Port<ReadService<T>> {
    public:
      friend class HasPort;

      ReadPort() = delete;
      ReadPort(ReadPort const & other) = delete;

      connection_type get_type() const override { return READ; }

      std::string type_name() const override;

      const T default_value;

      T read() const;
      inline T operator()() const { return read(); };

    private:
      ReadPort(HasPort* container, std::string const & name, T default_value);
      ~ReadPort() noexcept;
    };

  }
} /* namespace mauve */

#include "ipp/ReadPort.ipp"

#endif
