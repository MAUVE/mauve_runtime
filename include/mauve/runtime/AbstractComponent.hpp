/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_COMPONENT_HPP
#define MAUVE_RUNTIME_ABSTRACT_COMPONENT_HPP

#include "Configurable.hpp"
#include "WithName.hpp"
#include "WithAbstractShell.hpp"
#include "WithAbstractCore.hpp"
#include "WithAbstractFSM.hpp"
#include "WithLogger.hpp"
#include "common.hpp"

#include <string>
#include <iostream>
#include <pthread.h>

namespace mauve {
  namespace runtime {

    class Task;
    class AbstractState;
    class ComponentLogger;

    /**
    * Abstract Component class
    */
    class AbstractComponent
    : virtual public Configurable
    , virtual public WithAbstractShell
    , virtual public WithAbstractCore
    , virtual public WithAbstractFSM
    , virtual public WithLogger
    , virtual public WithName
    {
    public:
      friend class Task;

      /** Constructor.
       * \param name component name
       */
      AbstractComponent(std::string const & name);
      /** Destructor */
      virtual ~AbstractComponent();

      /** Get component name */
      inline std::string name() const override final { return _name; };
      /** Get component type name */
      std::string type_name() const;

      /** Get component current State */
      virtual AbstractState * current_state() = 0;

      /** Check if the component is empty (i.e. not built) */
      virtual bool is_empty     () const = 0;
      /** Check if the component is created */
      virtual bool is_created   () const = 0;
      /** Check if the component is activated */
      virtual bool is_activated () const = 0;
      /** Check if the component is running */
      virtual bool is_running   () const = 0;

      /** Clear the component */
      virtual bool clear() = 0;

      /** Disconnect the component */
      virtual void disconnect() = 0;

      /** Make one step on the component FSM */
      virtual bool step() = 0;

      /** Get the component current time */
      virtual time_ns_t get_time() const = 0;

      /**
       * Get the priority of the component real-time task
       * \return \a priority
       **/
      virtual int get_priority() const = 0;

      /**
       * Sets the real-time task priority.
       * 1 is the lowest priority and 99 the most prioritary.
       * The priority can be changed iff the component is neither activated nor running.
       * The default value is 1.
       * \param priority in 1-99
       **/
      virtual bool set_priority(int priority) = 0;

      /**
       * Get the cpu mapping of the component task.
       * \return \a cpu mapping
       **/
      virtual int get_cpu() const = 0;

      /**
       * Set the affinity of the task of the component to a single processor.
       * The cpu mapping can be changed iff the component is neither activated nor running.
       * The default value is 0.
       * Cpu id is checked.
       * \param cpu cpu mapping of the component task (0..nproc-1)
       * \return true if cpu mapping can be changed.
       **/
      virtual bool set_cpu(int cpu) = 0;

    protected:
      ComponentLogger* logger;

    private:
      std::string _name;
      virtual bool run() = 0;
      /** Set the task executing this component */
      virtual void set_task(Task* task) = 0;
    };

  } // namespace runtime
} // namespace mauve

#endif // MAUVE_RUNTIME_ABSTRACT_COMPONENT_HPP
