/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_PORT_HPP
#define MAUVE_RUNTIME_ABSTRACT_PORT_HPP

#include <string>
#include <vector>

namespace mauve {
  namespace runtime {

    /** Port types */
    enum connection_type { EVENT, READ, WRITE, CALL };

    class Service;

    /**
    * Abstract Port class
    */
    class AbstractPort {
    public:
      AbstractPort() = delete;
      /** Constructor
       * \param name port name
       */
      AbstractPort(std::string const & name);
      /** Constructor
       * \param other a port to construct from
       */
      AbstractPort(const AbstractPort & other) = delete;
      virtual ~AbstractPort() noexcept;

      /** Port name */
      const std::string name;

      /** Get port type */
      virtual connection_type get_type() const = 0;

      /** Get port type name */
      virtual std::string type_name() const = 0;

      /** Check if the port is connected */
      virtual bool is_connected() const = 0;
      /** Disconnect the port */
      virtual bool disconnect() = 0;

      /** Connect the port to a service */
      virtual bool connect_service (Service * service) = 0;

      /** Get the list of connected services */
      virtual std::vector<Service*> connected_services() const = 0;

      /** Get the number of connected services */
      virtual std::size_t connections_size() const = 0;

      /** Get connected Service according to the index */
      virtual Service* get_connection(int index) const = 0;
    };

  }
} /* namespace mauve */

#endif
