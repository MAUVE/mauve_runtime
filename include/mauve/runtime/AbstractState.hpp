/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_STATE_HPP
#define MAUVE_RUNTIME_ABSTRACT_STATE_HPP

#include <iostream>
#include <string>

#include "common.hpp"

namespace mauve {
  namespace runtime {

    class AbstractFiniteStateMachine;

    /**
    * Abstract State class
    */
    class AbstractState {
    public:
      friend AbstractFiniteStateMachine;

      /** State name */
      std::string const name;

      AbstractState() = delete;
      AbstractState(AbstractState const & other) = delete;
      AbstractState & operator=(AbstractState const & other) = delete;

      /** Check if the state is an Execution state */
      virtual bool is_execution() const = 0;
      /** Check if the state is a Synchronization state */
      virtual bool is_synchronization() const = 0;

      /**
       * Get the clock value of the SynchroState "name".
       * \return the clock value of the SynchroState or 0 if the state is not a SynchroState
       */
      virtual time_ns_t get_clock() const = 0;

      /**
       * Change the clock value of the SynchroState "name".
       * \return true if the FSM is not configured and if a SynchroState named "name" exists, false otherwise
       */
      virtual bool set_clock(time_ns_t clock) = 0;

      /** Return the number of transitions */
      virtual std::size_t get_next_size() const = 0;

      /** Return the next state according to the index */
      virtual AbstractState* get_next_state(int index) const = 0;

      /** Return the state as a string */
      virtual std::string to_string() const = 0;

    protected:
      AbstractState(AbstractFiniteStateMachine* container, std::string const & name);
      virtual ~AbstractState() noexcept;

      const AbstractFiniteStateMachine* container;
    };

    std::ostream & operator<<(std::ostream & out, AbstractState const & state);

}} /* namespace mauve */

#endif
