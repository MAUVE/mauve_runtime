/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_PERIODICSTATEMACHINE_HPP
#define MAUVE_RUNTIME_PERIODICSTATEMACHINE_HPP

#include "FiniteStateMachine.hpp"

namespace mauve {
  namespace runtime {

	/**
	 * Defines a Periodic State Machine.
   * \tparam SHELL Shell type
   * \tparam CORE Core type
	 */
	template <typename SHELL, typename CORE>
	struct PeriodicStateMachine : public FiniteStateMachine<SHELL, CORE> {
	public:
    /** Property to set the PSM period */
    Property<time_ns_t>& period = this->mk_property("period", sec_to_ns(1));
  private:
    /** Execution state.
     * Executes the update fun of the core */
    ExecState<CORE>& exec_state = this->mk_execution("E", &CORE::update);
    /** Synchronization state */
    SynchroState<CORE>& sync_state = this->mk_synchronization("S", period);
	protected:
    /** Configure the PSM.
     * Set the \a sync_state clock according to \a period.
     */
	  virtual bool configure_hook() override;
	};

  }
}

#include "ipp/PeriodicStateMachine.ipp"

#endif // MAUVE_RUNTIME_PERIODICSTATEMACHINE_HPP
