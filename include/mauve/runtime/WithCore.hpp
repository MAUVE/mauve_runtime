/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WITH_CORE_HPP
#define MAUVE_RUNTIME_WITH_CORE_HPP

#include "WithShell.hpp"

namespace mauve {
  namespace runtime {

    /** Object with a Core
     * \tparam CORE Core type
     */
    template <typename CORE>
    struct WithCore {
      /** Get the Core */
      virtual CORE & core     () const = 0;
      /** Get a pointer to the Core */
      virtual CORE * core_ptr () const = 0;
      /** Clear the Core */
      virtual bool clear_core ()       = 0;
    };

    /** Objects wite a Shell and a Core
     * \tparam SHELL Shell type
     * \tparam CORE Core type
     */
    template <typename SHELL, typename CORE>
    struct WithShellAndCore: virtual public WithShell<SHELL>, virtual public WithCore<CORE> {
    };

  };
}

#endif
