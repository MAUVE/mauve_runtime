/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_EVENT_PORT_HPP
#define MAUVE_RUNTIME_EVENT_PORT_HPP

#include "Port.hpp"

namespace mauve {
  namespace runtime {

    class EventService;

    class EventPort final: public Port<EventService> {
    public:
      friend class HasPort;

      EventPort() = delete;
      EventPort(EventPort const & other) = delete;

      connection_type get_type() const override { return EVENT; }

      std::string type_name() const override { return ""; }

      void react() const;
      inline void operator()() const { react(); };

    private:
      EventPort(HasPort* container, std::string const & name);
      ~EventPort() noexcept;
    };

  }
} // namespace

#endif
