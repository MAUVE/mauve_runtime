/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_PORT_HPP
#define MAUVE_RUNTIME_HAS_PORT_HPP

#include "Configurable.hpp"
#include <string>
#include <vector>

namespace mauve {
  namespace runtime {

    class AbstractPort;

    class EventPort;

    template <typename T>
    class ReadPort;

    template <typename T>
    class WritePort;

    template <typename R, typename ...P>
    class CallPort;

    class HasPort : virtual public Configurable {
    public:
      HasPort();
      virtual ~HasPort() noexcept;

      /**
       * Get the ports of the shell.
       * \return a vector of ports
       */
      const std::vector<AbstractPort*> get_ports() const;

      AbstractPort* get_port(std::string const & name) const;

      std::size_t get_ports_size() const { return ports.size(); }

      AbstractPort* get_port(int index) const;

      void disconnect();

    protected:
      /**
       * Create a new port in this shell.
       * \tparam PORT Port type
       * \tparam PARAM List of Port parameters types
       * \return a reference to a Port object
       * \param name name of the port
       * \param parameters list of port parameters
       */
      template <typename PORT, typename ... PARAM>
      PORT & mk_port(std::string const & name, PARAM ... parameters);

      EventPort & mk_event_port(std::string const & name);

      template <typename T>
      ReadPort<T> & mk_read_port(std::string const & name, T default_value);

      template <typename T>
      WritePort<T> & mk_write_port(std::string const & name);

      template <typename R, typename ...P>
      CallPort<R, P...> mk_call_port(std::string const & name, R default_value);

    private:
      std::vector<AbstractPort *> ports;
    };

    // -------------------- Exceptions --------------------
    /** Exception for Already Defined Ports */
    struct AlreadyDefinedPort: public std::exception {
      AlreadyDefinedPort(std::string const & name) throw() : name(name) {}
      /** Exception name */
      const std::string name;
      /** Exception explanation */
      virtual const char* what() const throw() {
        std::string message = "Already Defined Port " + name;
        return message.c_str();
      }
    };

  }
}

#include "mauve/runtime/ipp/HasPort.ipp"

#endif
