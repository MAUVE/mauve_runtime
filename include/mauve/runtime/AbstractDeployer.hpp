/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_DEPLOYER_HPP
#define MAUVE_RUNTIME_ABSTRACT_DEPLOYER_HPP

#include <signal.h>
#include <map>
#include <vector>
#include <string>

#include "common.hpp"
#include "WithLogger.hpp"

#define DEPLOYER_SIG SIGINT

namespace mauve {
  namespace runtime {

    class Architecture;
    class AbstractComponent;
    class AbstractDeployer;
    class Task;
    class DeployerLogger;

    template <typename A>
    class Manager;

    /**
    * Build the application Deployer from an architecture.
    * \tparam ARCHI Architecture type
    * \param architecture The architecture executed by the Deployer
    * \param manager A manager of the architecture
    * \return a pointer on the application Deployer.
    */
    template <typename ARCHI>
    AbstractDeployer* mk_abstract_deployer(ARCHI * architecture, Manager<ARCHI> * manager = nullptr);

    /**
    * Abstract Deployer class
    */
    class AbstractDeployer : public WithLogger {
    public:
      /** Get the Deployer instance.
      * \return a pointer on the deployer instance
      */
      static AbstractDeployer* instance() { return deployer; }
      /** Destructor. */
      virtual ~AbstractDeployer() noexcept;

      // ---------- Architecture ----------

      /**
      * Get the deployer architecture.
      * \return a pointer on the deployed architecture
      */
      virtual Architecture* get_architecture() = 0;

      // ---------- Manager ----------

      /**
      * Get the list of Manager actions.
      * \return a vector of the registered action names.
      */
      virtual std::vector<std::string> manager_actions() const = 0;
      /**
      * Apply a Manager action.
      * \param name the name of the action to apply
      * \return true if the action has succeeded.
      */
      virtual bool manager_apply(std::string name) = 0;

      // ---------- Components & Tasks ----------

      /**
      * Get the task associated to a component.
      * \param component the component
      * \return the task executing \a component
      */
      Task* get_task(AbstractComponent * component);

      // ---------- Start/Stop ----------

      /**
      * Get current deployer time.
      * \return currect time.
      */
      time_ns_t now() const;

      /**
      * Get deployer reference time.
      * \return reference time
      */
      time_ns_t get_time() const;

      /**
      * Create all tasks.
      * \return true if all tasks are created for all components.
      */
      bool create_tasks();

      /**
      * Create one task.
      * \param component the component corresponding to the task to create
      * \return true if the task is created.
      */
      bool create_task(AbstractComponent * component);

      /**
      * Activate all tasks.
      * \return true if all tasks are activated.
      */
      bool activate();

      /**
      * Activate one task.
      * \param component the component corresponding to the task to activate
      * \return true if the task is activated.
      */
      bool activate(AbstractComponent * component);

      /**
      * Activate one task.
      * \param name name of the task/component to activate
      * \return true if the task is activated.
      */
      bool activate(std::string name);

      /**
      * Start all tasks.
      * \return true if all tasks are started.
      */
      bool start();

      /**
      * Start a task.
      * \param component component of the task to activate
      * \param start_time reference time at which to start the task
      * \return true if the task is started.
      */
      bool start(AbstractComponent * component, time_ns_t start_time);

      /**
      * Start a task.
      * \param name name of the task/component to activate
      * \param start_time reference time at which to start the task
      * \return true if the task is started.
      */
      bool start(std::string name, time_ns_t start_time);

      /**
      * Start only the Deployer.
      * Tasks have to be started individually.
      * \return true if the deployer is started.
      */
      bool start_deployer();
      /**
      * Loop
      */
      void loop();

      /**
      * Stop all.
      */
      void stop();

      /**
      * Stop task
      */
      bool stop(AbstractComponent * component);

      /** Stop task by name */
      bool stop(std::string name);

      /** Clear the tasks. */
      void clear_tasks();

      /**
      * Clear component task
      */
      bool clear_task(AbstractComponent * component);

      /** Access to deployer logger */
      virtual AbstractLogger& logger() const override { return *_logger; };

    protected:
      /** Constructor. */
      AbstractDeployer();
      /** The singleton Deployer instance. */
      static AbstractDeployer* deployer;
      /** The Deployer logger. */
      DeployerLogger* _logger;

    private:
      template <typename ARCHI>
      friend AbstractDeployer* mk_abstract_deployer(ARCHI* a, Manager<ARCHI>* m);

      std::map<AbstractComponent*, Task*> tasks;
      time_ns_t start_time;
      sigset_t sig_set;

    };

    /**
    * Create the Deployer for access from the python binding.
    */
    void mk_python_deployer();

  }
}

#endif // MAUVE_RUNTIME_ABSTRACT_DEPLOYER_HPP
