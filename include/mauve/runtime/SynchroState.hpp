/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SYNCHRO_STATE_HPP
#define MAUVE_RUNTIME_SYNCHRO_STATE_HPP

#include "common.hpp"
#include "State.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <functional>

namespace mauve {
  namespace runtime {

  template <typename CORE>
  class SynchroState final : public State<CORE> {

  public:
    template <typename S, typename C>
    friend class FiniteStateMachine;

    SynchroState() = delete;
    SynchroState(SynchroState<CORE> const & other) = delete;
    SynchroState<CORE> & operator=(SynchroState<CORE> const & other) = delete;

    bool is_execution() const override;
    bool is_synchronization() const override;

    time_ns_t get_clock() const override;
    bool set_clock(time_ns_t clock) override;

    std::size_t get_next_size() const override { return 1; }

    AbstractState* get_next_state(int index) const override;

    std::string to_string() const override;

  private:
    SynchroState(AbstractFiniteStateMachine* container, std::string const & name, time_ns_t clock);
    ~SynchroState() noexcept;

    std::vector<State<CORE>*> next_states() const override;

    time_ns_t clock;
    State<CORE> * run(CORE * core) override;

  };

}}

#include "ipp/SynchroState.ipp"

#endif
