/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_DEPLOYER_INTERFACE_HPP
#define MAUVE_RUNTIME_DEPLOYER_INTERFACE_HPP

#include <string>

#include "AbstractDeployer.hpp"
#include "AbstractComponent.hpp"
#include "AbstractResource.hpp"
#include "AbstractInterface.hpp"
#include "Shell.hpp"
#include "AbstractCore.hpp"
#include "AbstractFiniteStateMachine.hpp"
#include "AbstractState.hpp"
#include "Property.hpp"

extern "C" {

  char* string_cache = nullptr;

  void free_string_cache();
  char* add_string_cache(std::string str);

  // Deployer
  //--------------------------------------------------

  long deployer_now();
  long deployer_get_time();

  int deployer_create_tasks();
  int deployer_create_task(int cpu_id);

  void deployer_clear_tasks();
  int  deployer_clear_task(int cpu_id);

  int deployer_activate();
  int deployer_activate_component(int cpt_id);

  int deployer_start();
  int deployer_start_component(int cpt_id, long start_time);

  void deployer_stop();
  int  deployer_stop_component(int cpt_id);

  // Logger
  //--------------------------------------------------

  void logger_initialize(const char* config);

  // Architecture
  //--------------------------------------------------
  const char* architecture_type_name();

  int architecture_is_configured();
  int architecture_configure();
  void architecture_cleanup();

  int architecture_components_number();
  int architecture_resources_number();

  // Component
  //--------------------------------------------------

  mauve::runtime::AbstractComponent* architecture_component(int cpt_id);

  const char* component_name(int cpt_id);
  const char* component_type_name(int cpt_id);

  int component_is_configured(int cpt_id);
  int component_configure(int cpt_id);
  void component_cleanup(int cpt_id);

  int component_is_empty(int cpt_id);
  int component_is_created(int cpt_id);
  int component_is_activated(int cpt_id);
  int component_is_running(int cpt_id);

  int component_priority(int cpt_id);
  int component_set_priority(int cpt_id, int priority);

  int component_cpu(int cpt_id);
  int component_set_cpu(int cpt_id, int cpu);

  long component_clock(int cpt_id);

  // TODO: const char* component_current_state_name(int cpt_id);

  // Component Shell
  //--------------------------------------------------

  mauve::runtime::Shell* component_shell(int cpt_id);

  const char* component_shell_type_name(int cpt_id);

  int component_shell_is_configured(int cpt_id);
  int component_shell_configure(int cpt_id);
  void component_shell_cleanup(int cpt_id);

  // Component Shell Property
  //--------------------------------------------------

  mauve::runtime::AbstractProperty* component_shell_property(int cpt_id, int prop_id);

  int component_shell_properties_number(int cpt_id);

  const char* component_shell_property_name(int cpt_id, int prop_id);
  const char* component_shell_property_type_name(int cpt_id, int prop_id);
  int component_shell_property_type(int cpt_id, int prop_id);


  long component_shell_property_get_integral_value(int cpt_id, int prop_id);
  int component_shell_property_set_integral_value(int cpt_id, int prop_id, long value);
  
  double component_shell_property_get_floating_value(int cpt_id, int prop_id);
  int component_shell_property_set_floating_value(int cpt_id, int prop_id, double value);
  
  const char* component_shell_property_get_string_value(int cpt_id, int prop_id);
  int component_shell_property_set_string_value(int cpt_id, int prop_id, const char* value);

  // Component Shell Port
  //--------------------------------------------------

  mauve::runtime::AbstractPort* component_shell_port(int cpt_id, int port_id);

  int component_shell_ports_number(int cpt_id);

  const char* component_shell_port_name(int cpt_id, int port_id);
  const char* component_shell_port_type_name(int cpt_id, int port_id);
  int component_shell_port_type(int cpt_id, int port_id);

  int component_shell_port_is_connected(int cpt_id, int port_id);
  int component_shell_port_connections_number(int cpt_id, int port_id);
  int component_shell_port_connection_resource(int cpt_id, int port_id, int serv_id);
  int component_shell_port_connection_service(int cpt_id, int port_id, int serv_id);

  int component_shell_port_connect(int cpt_id, int port_id, int res_id, int serv_id);
  int component_shell_port_disconnect(int cpt_id, int port_id);

  // Component Core
  //--------------------------------------------------

  mauve::runtime::AbstractCore* component_core(int cpt_id);

  const char* component_core_type_name(int cpt_id);

  int component_core_is_configured(int cpt_id);
  int component_core_configure(int cpt_id);
  void component_core_cleanup(int cpt_id);

  // Component Core Property
  //--------------------------------------------------

  mauve::runtime::AbstractProperty* component_core_property(int cpt_id, int prop_id);

  int component_core_properties_number(int cpt_id);

  const char* component_core_property_name(int cpt_id, int prop_id);
  const char* component_core_property_type_name(int cpt_id, int prop_id);
  int component_core_property_type(int cpt_id, int prop_id);

  long component_core_property_get_integral_value(int cpt_id, int prop_id);
  int component_core_property_set_integral_value(int cpt_id, int prop_id, long value);
  
  double component_core_property_get_floating_value(int cpt_id, int prop_id);
  int component_core_property_set_floating_value(int cpt_id, int prop_id, double value);
  
  const char* component_core_property_get_string_value(int cpt_id, int prop_id);
  int component_core_property_set_string_value(int cpt_id, int prop_id, const char* value);

  // Component FSM
  //--------------------------------------------------

  mauve::runtime::AbstractFiniteStateMachine* component_fsm(int cpt_id);

  const char* component_fsm_type_name(int cpt_id);

  int component_fsm_is_configured(int cpt_id);
  int component_fsm_configure(int cpt_id);
  void component_fsm_cleanup(int cpt_id);

  mauve::runtime::AbstractState* component_fsm_state(int cpt_id, int state_id);

  int component_fsm_states_number(int cpt_id);
  int component_fsm_state_is_execution(int cpt_id, int state_id);
  int component_fsm_state_is_synchronization(int cpt_id, int state_id);
  const char* component_fsm_state_name(int cpt_id, int state_id);

  long component_fsm_state_clock(int cpt_id, int state_id);

  int component_fsm_state_next_number(int cpt_id, int state_id);
  int component_fsm_state_next(int cpt_id, int state_id, int next_id);

  // Component FSM Property
  //--------------------------------------------------

  mauve::runtime::AbstractProperty* component_fsm_property(int cpt_id, int prop_id);

  int component_fsm_properties_number(int cpt_id);

  const char* component_fsm_property_name(int cpt_id, int prop_id);
  const char* component_fsm_property_type_name(int cpt_id, int prop_id);
  int component_fsm_property_type(int cpt_id, int prop_id);

  long component_fsm_property_get_integral_value(int cpt_id, int prop_id);
  int component_fsm_property_set_integral_value(int cpt_id, int prop_id, long value);
  
  double component_fsm_property_get_floating_value(int cpt_id, int prop_id);
  int component_fsm_property_set_floating_value(int cpt_id, int prop_id, double value);
  
  const char* component_fsm_property_get_string_value(int cpt_id, int prop_id);
  int component_fsm_property_set_string_value(int cpt_id, int prop_id, const char* value);

  // Resource
  //--------------------------------------------------

  mauve::runtime::AbstractResource* architecture_resource(int res_id);

  const char* resource_name(int res_id);
  const char* resource_type_name(int res_id);

  int resource_is_configured(int res_id);
  int resource_configure(int res_id);
  void resource_cleanup(int res_id);

  // Resource Shell
  //--------------------------------------------------

  mauve::runtime::Shell* resource_shell(int res_id);

  const char* resource_shell_type_name(int res_id);

  int resource_shell_is_configured(int res_id);
  int resource_shell_configure(int res_id);
  void resource_shell_cleanup(int res_id);

  // Resource Shell Property
  //--------------------------------------------------

  mauve::runtime::AbstractProperty* resource_shell_property(int res_id, int prop_id);

  int resource_shell_properties_number(int res_id);

  const char* resource_shell_property_name(int res_id, int prop_id);
  const char* resource_shell_property_type_name(int res_id, int prop_id);
  int resource_shell_property_type(int res_id, int prop_id);


  long resource_shell_property_get_integral_value(int res_id, int prop_id);
  int resource_shell_property_set_integral_value(int res_id, int prop_id, long value);
  
  double resource_shell_property_get_floating_value(int res_id, int prop_id);
  int resource_shell_property_set_floating_value(int res_id, int prop_id, double value);
  
  const char* resource_shell_property_get_string_value(int res_id, int prop_id);
  int resource_shell_property_set_string_value(int res_id, int prop_id, const char* value);

  // Resource Shell Port
  //--------------------------------------------------

  mauve::runtime::AbstractPort* resource_shell_port(int res_id, int port_id);

  int resource_shell_ports_number(int res_id);

  const char* resource_shell_port_name     (int res_id, int port_id);
  const char* resource_shell_port_type_name(int res_id, int port_id);
  int         resource_shell_port_type     (int res_id, int port_id);

  int resource_shell_port_is_connected(int res_id, int port_id);
  int resource_shell_port_connections_number(int res_id, int port_id);
  int resource_shell_port_connection_resource(int res_id, int port_id, int serv_id);
  int resource_shell_port_connection_service(int res_id, int port_id, int serv_id);

  int resource_shell_port_connect(int src_id, int port_id, int dst_id, int serv_id);
  int resource_shell_port_disconnect(int cpt_id, int port_id);

  // Resource Core
  //--------------------------------------------------

  mauve::runtime::AbstractCore* resource_core(int res_id);

  const char* resource_core_type_name(int res_id);

  const char* resource_core_to_string(int res_id);

  int resource_core_is_configured(int res_id);
  int resource_core_configure(int res_id);
  void resource_core_cleanup(int res_id);

  // Resource Core Property
  //--------------------------------------------------

  mauve::runtime::AbstractProperty* resource_core_property(int res_id, int prop_id);

  int resource_core_properties_number(int res_id);

  const char* resource_core_property_name(int res_id, int prop_id);
  const char* resource_core_property_type_name(int res_id, int prop_id);
  int resource_core_property_type(int res_id, int prop_id);

  long resource_core_property_get_integral_value(int res_id, int prop_id);
  int resource_core_property_set_integral_value(int res_id, int prop_id, long value);
  
  double resource_core_property_get_floating_value(int res_id, int prop_id);
  int resource_core_property_set_floating_value(int res_id, int prop_id, double value);
  
  const char* resource_core_property_get_string_value(int res_id, int prop_id);
  int resource_core_property_set_string_value(int res_id, int prop_id, const char* value);

  // Resource Interface
  //--------------------------------------------------

  mauve::runtime::AbstractInterface* resource_interface(int res_id);

  int  resource_interface_is_configured(int res_id);
  int  resource_interface_configure    (int res_id);
  void resource_interface_cleanup      (int res_id);

  // Resource Interface Services
  //--------------------------------------------------

  mauve::runtime::Service* resource_interface_service(int res_id, int serv_id);

  int resource_interface_services_number(int res_id);

  const char* resource_interface_service_name     (int res_id, int serv_id);
  const char* resource_interface_service_type_name(int res_id, int serv_id);
  int         resource_interface_service_type     (int res_id, int serv_id);
};

#endif
