/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_FINITE_STATE_MACHINE_HPP
#define MAUVE_RUNTIME_FINITE_STATE_MACHINE_HPP

#include "common.hpp"
#include "AbstractFiniteStateMachine.hpp"
#include "WithLogger.hpp"
#include "WithHook.hpp"
#include "FSMContainer.hpp"

#include <string>
#include <vector>
#include <functional>

namespace mauve {
  namespace runtime {

    template <typename T>
    class State;

    template <typename T>
    class SynchroState;

    template <typename T>
    class ExecState;

    template <typename SHELL, typename CORE>
    class FiniteStateMachine
    : public AbstractFiniteStateMachine
    , virtual public WithLogger
    , virtual public WithHook
    {
    public:
      using Guard_t  = typename ExecState<CORE>::Guard_t;
      using Update_t = typename ExecState<CORE>::Update_t;

      template <typename S, typename C, typename F>
      friend class Component;

      FiniteStateMachine(FiniteStateMachine const & other) = delete;
      FiniteStateMachine<SHELL, CORE> & operator=(FiniteStateMachine<SHELL, CORE> const & other) = delete;

      std::string shell_type_name() const override;
      std::string core_type_name () const override;

      inline AbstractLogger& logger() const override { return _container->logger(); };

      inline bool is_configured() const override final { return _configured; }

      inline bool configure() override final { return _container->configure_fsm(); }
      inline void cleanup()   override final { _container->cleanup_fsm(); }

      std::size_t get_states_size() const override { return states.size(); }
      AbstractState* get_state(std::string const & name) const override;
      AbstractState* get_state(int index) const override;
      int get_state_index(const AbstractState* state) const override;

      bool check_reachable() const;

    protected:
      FiniteStateMachine();
      virtual ~FiniteStateMachine() noexcept;

      ExecState<CORE> & mk_execution(std::string const & name, Update_t fun);
      SynchroState<CORE> & mk_synchronization(std::string const & name, time_ns_t clock);

      void set_initial(State<CORE> & state);
      void set_next(State<CORE> & state, State<CORE> & next);
      void mk_transition(ExecState<CORE> & state, Guard_t guard, State<CORE> & next);

      inline SHELL & shell() const { return _container->shell(); };
      inline CORE  & core () const { return _container->core(); };

    private:
      FSMContainer<SHELL, CORE>* _container;
      bool _configured;
      State<CORE> * initial;
      std::vector<State<CORE> *> states;

      bool _configure();
      void _cleanup();
    };

    /** Exception for Already Defined States */
    struct AllreadyDefinedState: public std::exception {
      AllreadyDefinedState(std::string const & name) throw() : name(name) {}
      /** Exception name */
      const std::string name;
      /** Exception explanation */
      virtual const char* what() const throw() {
        std::string message = "Allready Defined State " + name;
        return message.c_str();
      }
    };

  }
} // namespace

#include "ipp/FiniteStateMachine.ipp"

#endif
