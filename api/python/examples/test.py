'''
# Copyright 2017 ONERA
#
# This file is part of the MAUVE Runtime project.
#
# MAUVE Runtime is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE Runtime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
'''
# LD_LIBRARY_PATH=${PWD}/devel/lib/ python3 -i ../api/python/examples/test.py
import mauve_runtime

mauve_runtime.load_deployer("libtest_deployer.so")
deployer = mauve_runtime.deployer
architecture = mauve_runtime.deployer.architecture

print(architecture)
print(architecture.configure())
# print(archi.components)

cpt_1 = architecture.cpt_1
cpt_2 = architecture.cpt_2

deployer.create_tasks()
task_1 = deployer.task(cpt_1)
task_2 = deployer.task(cpt_2)

# print(cpt_1)
# print(cpt_1.shell)
# print(cpt_1.shell.is_configured)
